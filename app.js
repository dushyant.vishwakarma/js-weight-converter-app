// Output 
const output = document.getElementById('output');
output.style.visibility = 'hidden';

// Get Pounds
document.getElementById('lbsInput').addEventListener('input',(e) => {
    
    output.style.visibility = 'visible';

    let lbs = e.target.value;

    if(lbs === ''){
        output.style.visibility = 'hidden';
    }

    // Grams Output
    document.getElementById('gramsOutput').innerHTML = lbs/0.0022046;

    // Kilograms Output
    document.getElementById('kgOutput').innerHTML = lbs/2.205;

    // Ounces Output
    document.getElementById('ouncesOutput').innerHTML = lbs * 16;
});

